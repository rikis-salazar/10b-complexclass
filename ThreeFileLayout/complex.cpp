// Three file setup: complex.cpp 
// 
// This is the companion file to 'complex.h'. Some programmers
// and/or IDE's use the file extension '.hpp' instead of '.cpp'
// (e.g., complex.hpp vs complex.cpp) to make it clear that it
// is not a standalone program, but rather the "other part" of
// a library file.



// While it is possible to 'include' other libraries, we should
// have done so already in 'complex.h'. Only in very few, very
// specific cases you want to include a library here but not
// in the corresponding '.h' file.

#include "complex.h" // 'Complex' stuff

// using namespace std;     // <-- BAD IDEA!!! Avoid.



// Code, code and more code. The idea is that if a programmer
// comes across our library, she should look into the '.h'
// file first to "get and idea of how it works". If she so 
// chooses, then can access the '.cpp' definition file
// to understand the particulars of the implementation.



// Constructors
Complex::Complex(){ 
    this->real = 0.;
    imaginary = 0.;     

}

Complex::Complex( double x ){
    real = x ;
    imaginary = 0. ;
}

Complex::Complex( double x, double y ) : real(x), imaginary(y) { }





// getters and setters
double Complex::get_real() const {
    return real;
}

double Complex::get_imaginary() const {
    return imaginary;
}

void Complex::set_real( double r ){
    real = r ;
    return;
}

void Complex::set_imaginary( double i ){
    imaginary = i;
    return;
}





// Miscellaneous functions
void Complex::to_console() const { 
    std::cout << "Real: " << real << ", Imaginary: " << imaginary << ".\n";
    return;
}

void Complex::to_console2() const { 
    std::cout << "Real: " << real_part() << ", Imaginary: " << imaginary_part() << ".\n";
    return;
}





// getters and setters (combined)
double Complex::real_part() const {
    return real;
}

double& Complex::real_part() {
    return real;
}

double Complex::imaginary_part() const {
    return imaginary;
}

double& Complex::imaginary_part() {
    return imaginary;
}






// Binary Math operators (MEMBERS)
Complex& Complex::operator+=( const Complex& z ){
    real += z.real;
    imaginary += z.imaginary;

    return *this; 
}   





// Unary Math operators (MEMBERS)
Complex Complex::operator-() const {
    return Complex( -real, -imaginary );
}

Complex& Complex::operator++(){
    return *this += 1;   
}

Complex Complex::operator++( int unused ){
    Complex copy_of_this = *this ;
    *this += 1;

    return copy_of_this;
}





// MEMBER helper functions
int Complex::compare( const Complex& w ) const {
    double A = real, B = imaginary;
    double C = w.real, D = w.imaginary;

    if ( A < C ) return 1;
    if ( (A == C) && (B < D) ) return 1;
    if ( (A == C) && (B == D) ) return 0;
    return -1;
}





// Extra NON-MEMBER function(s)
void print( const Complex& z ){ 
    z.to_console();
    return;
}



// Output operator
std::ostream& operator<<( std::ostream& out , const Complex& z) {
    out << z.real ;

    // If 'z.imaginary' is negative, use '-' instead of 
    // '+' and output the absolute value of 'z.imaginary'.
    if ( z.imaginary < 0 )
        out << " - " << -z.imaginary ;
    else
        out << " + " << z.imaginary ; 
            
    out << "i";

    return out;
}



// Non-member math operators
Complex operator+( const Complex& w, const Complex& z ){
    Complex copy_of_w( w );     
    return copy_of_w += z;
}



// Logic operators NON-MEMBER NON-FRIEND
bool operator<( const Complex& w , const Complex& z ){
    return w.compare(z) > 0 ;
}

bool operator>( const Complex& w , const Complex& z ){
    return w.compare(z) < 0 ;
}

bool operator==( const Complex& w , const Complex& z ){
    return w.compare(z) == 0 ;
}

bool operator!=( const Complex& w , const Complex& z ){
    return w.compare(z) != 0 ;
}

bool operator<=( const Complex& w , const Complex& z ){
    return w.compare(z) >= 0 ;
}

bool operator>=( const Complex& w , const Complex& z ){
    return w.compare(z) <= 0 ;
}
