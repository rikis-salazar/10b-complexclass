// Three file setup: 'complex.h'
// This file is know as a library file. It can be 'included'
// in other programs (e.g., complex-driver.cpp) just like
// "standard" libraries like <iostream> and <string>.
//
// One thing to notice is that instead of using < and >
// to delimit the name of the file, we need to use " ".
// This lets the compiler not to bother looking for this
// library file in a "standard" location. If we want the
// compiler to locate this file, extra configuration steps
// are needed. This is IDE dependent. This is, the steps
// we would take when using 'xcode' might be different than
// the ones needed while using Visual Studio. 

// Please make sure you find out about this extra steps 
// required for your specific choice of IDE. 



// Include safeguards
#ifndef SOME_RATHER_LONG_TAG_LIKE_COMPLEX_10C_W17
#define SOME_RATHER_LONG_TAG_LIKE_COMPLEX_10C_W17

// You can include other libraries, but you should try
// to only use the ones that are needed in this particular
// piece of code. In this case, notice that since we do not
// send info to a file (which is done in 'main()') we really
// do not need to include <fstream>.
//
// If we eliminate the output functions 'to_console()',
// 'print()', etc., then we can also remove the <iostream>
// library.

#include <iostream>  // std::cout in 'print()', 'to_console()'
#include <ostream>   // std::ostream in 'operator>>'
// #include <istream>   // should we decide to implement 'operator<<'


// Using "namespaces" in libraries is a recipe for disaster
// when working on large projects. We will keep this line in
// 'main()' but we will move it to the body of the function
// to reduce the scope of objects it can reach.
//
//     using namespace std;
//
// One disadvantage of this approach is that we have to use
// the full name of the functions and/or objects we are using.
// E.g. std::cout, std::string, std::ostream vs cout, string,
//      and ostream.



// Forward declaration
class Complex;



// To comment, or not to comment?
// You have been told since you took PIC 10A (or an equivalent)
// class, that you need to comment your code. While this is
// certainly a good piece of advice, it is much better to
// write code (including names of Classes and functions) that
// is self-descriptive. This also acts as a certain way to
// comment your code AND it reduces the points of maintenance
// of your code. If you change the behavior of a function
// and if the change is self-explanatory, then you can simply
// move on. On the other hand, with comments, you would have
// to modify your code and make sure your comments reflect
// the new behavior of the piece of code you modified (this
// is an additional point of maintenance for you).




// Non-member function declarations

// You might want to add some comments here... or you might
// notice that the names and the types in the functions below
// make it clear what to expect from this class.
void print( const Complex& ); 
std::ostream& operator<<( std::ostream& , const Complex& );
Complex operator+( const Complex& , const Complex& );



// Logic operators
bool operator<( const Complex& , const Complex& );
bool operator>( const Complex& , const Complex& );
bool operator==( const Complex& , const Complex& );
bool operator!=( const Complex& , const Complex& );
bool operator<=( const Complex& , const Complex& );
bool operator>=( const Complex& , const Complex& );


// Class interface
class Complex{
  private: 
    double real, imaginary;

  public:
    Complex();
    Complex( double );
    Complex( double , double );

    // Miscellaneous functions
    void to_console() const ;
    void to_console2() const ; 

    // getters and setters
    double get_real() const ;
    double get_imaginary() const ;  
    void set_real( double );             
    void set_imaginary( double );

    // Combined functionality of getters and setters.
    double real_part() const ;     
    double& real_part() ;   
    double imaginary_part() const ; 
    double& imaginary_part() ; 

    // Binary Math operators (MEMBERS)
    Complex& operator+=( const Complex& ); 

    // Unary Math operators (MEMBERS)
    Complex operator-() const ;
    Complex& operator++();
    Complex operator++( int );

    // MEMBER helper functions.
    int compare( const Complex& ) const ;

    // NON-MEMBERS (friends)
    friend std::ostream& operator<<( std::ostream& , const Complex& );
};

#endif
