// Three file setup. This is the so-called "driver".
// The main routine is located in this file. No 
// details about the implementation of 'Complex'
// should be found here.

#include <iostream>  // std::cout
#include <fstream>   // std::ofstream
#include "complex.h" // 'Complex' objects and functions


// This is fine for small projects, but let us restrict the
// scope (see 'main()' below).
//     using namespace std;



// main routine
int main(){

    using namespace std; // to use 'cout' instead of 'std::cout'
    // Better yet we can specify which names from the 'std'
    // namespace are the ones that we are ging to use.
    //     using std::cout;
    //     using std::string;
    //     using std::ofstream;
    // etc.

    cout << "Hola, mundo 'Complejo'.\n\n";

    Complex z1;
    Complex z2(3.14);
    Complex z3(1.2,3.45);

    cout << "z1 = " << z1 << '\n';
    cout << "z2 = " << z2 << '\n';
    cout << "z3 = " << z3 << "\n\n";

    cout << "z2 + Complex(0,1) = " << Complex(0,1) + z2 << '\n';
    cout << "4 - z3  = " << -( z3 + (-4) )<< '\n';

    z1++++;
    cout << "\nAfter z1++++, z1 = " << z1 << ".\nDoes this make sense?\n";

    ++++z2;
    cout << "\nAfter ++++z2, z2 = " << z2 << ".\nDoes this make sense?\n";

    cout << '\n';
    cout << z1 << " < " << z2 << " is: ";
    if ( z1 < z2 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << "\nYour turn. Write your own tests.\n";

    return 0;
}

