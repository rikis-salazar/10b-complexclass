// One file setup. Both, the interface and implementation
// of the 'Complex' class can be found here.

#include <iostream>  // std::cout
#include <ostream>   // std::ostream
#include <fstream>   // std::ofstream

using namespace std;

// Forward declaration
class Complex;

// Non-member function declarations
void print( const Complex& z ); 
ostream& operator<<( ostream& , const Complex& );
Complex operator+( const Complex& , const Complex& );

// Logic operators
bool operator<( const Complex& , const Complex& );
bool operator>( const Complex& , const Complex& );
bool operator==( const Complex& , const Complex& );
bool operator!=( const Complex& , const Complex& );
bool operator<=( const Complex& , const Complex& );
bool operator>=( const Complex& , const Complex& );


// Class interface
class Complex{
  private: 
    double real, imaginary;

  public:
    Complex();
    Complex( double );
    Complex( double , double );

    // Miscellaneous functions
    void to_console() const ;
    void to_console2() const ; 

    // getters and setters
    double get_real() const ;
    double get_imaginary() const ;  
    void set_real( double );             
    void set_imaginary( double );

    // Combined functionality of getters and setters.
    double real_part() const ;     
    double& real_part() ;   
    double imaginary_part() const ; 
    double& imaginary_part() ; 

    // Binary Math operators (MEMBERS)
    Complex& operator+=( const Complex& ); 

    // Unary Math operators (MEMBERS)
    Complex operator-() const ;
    Complex& operator++();
    Complex operator++( int );

    // MEMBER helper functions.
    int compare( const Complex& ) const ;

    // NON-MEMBERS (friends)
    friend ostream& operator<<( ostream& , const Complex& );
};


// Constructors
Complex::Complex(){ 
    this->real = 0.;
    imaginary = 0.;     

}
Complex::Complex( double x ){
    real = x ;
    imaginary = 0. ;
}
Complex::Complex( double x, double y ) : real(x), imaginary(y) { }


// getters and setters
double Complex::get_real() const {
    return real;
}
double Complex::get_imaginary() const {
    return imaginary;
}
void Complex::set_real( double r ){
    real = r ;
    return;
}
void Complex::set_imaginary( double i ){
    imaginary = i;
    return;
}


// Miscellaneous functions
void Complex::to_console() const { 
    cout << "Real: " << real << ", Imaginary: " << imaginary << ".\n";
    return;
}
void Complex::to_console2() const { 
    cout << "Real: " << real_part() << ", Imaginary: " << imaginary_part() << ".\n";
    return;
}


// getters and setters (combined)
double Complex::real_part() const {
    return real;
}
double& Complex::real_part() {
    return real;
}
double Complex::imaginary_part() const {
    return imaginary;
}
double& Complex::imaginary_part() {
    return imaginary;
}



// Binary Math operators (MEMBERS)
Complex& Complex::operator+=( const Complex& z ){
    real += z.real;
    imaginary += z.imaginary;
    return *this; 
}   

// Unary Math operators (MEMBERS)
Complex Complex::operator-() const {
    return Complex( -real, -imaginary );
}

Complex& Complex::operator++(){
    return *this += 1;   
}

Complex Complex::operator++( int unused ){
    Complex copy_of_this = *this ;
    *this += 1;
    return copy_of_this;
}


// MEMBER helper functions
int Complex::compare( const Complex& w ) const {
    double A = real, B = imaginary;
    double C = w.real, D = w.imaginary;

    if ( A < C ) return 1;
    if ( (A == C) && (B < D) ) return 1;
    if ( (A == C) && (B == D) ) return 0;
    return -1;
}



// main routine
int main(){

    Complex z1;
    Complex z2(3.14);
    Complex z3(1.2,3.45);

    cout << z1 << " < " << z2 << " is: ";
    if ( z1 < z2 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << z2 << " < " << z3 << " is: ";
    if ( z2 < z3 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << z2 << " <= " << z2 << " is: ";
    if ( z2 <= z2 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << z3 << " >= " << z1 << " is: ";
    if ( z3 >= z1 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << z1 << " == " << 0 << " is: ";
    if ( z1 == 0 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    cout << 3.14 << " != " << z2 << " is: ";
    if ( 3.14 != z2 ) cout << "TRUE";
    else cout << "FALSE";
    cout << '\n';

    return 0;
}


// Extra NON-MEMBER function(s)
void print( const Complex& z ){ 
    z.to_console();
    return;
}

ostream& operator<<( ostream& out , const Complex& z) {
    out << z.real ;

    // If 'z.imaginary' is negative, use '-' instead of 
    // '+' and output the absolute value of 'z.imaginary'.
    if ( z.imaginary < 0 )
        out << " - " << -z.imaginary ;
    else
        out << " + " << z.imaginary ; 
            
    out << "i";

    return out;
}


Complex operator+( const Complex& w, const Complex& z ){
    Complex copy_of_w( w );     
    return copy_of_w += z;
}


// Logic operators NON-MEMBER NON-FRIEND
bool operator<( const Complex& w , const Complex& z ){
    return w.compare(z) > 0 ;
}
bool operator>( const Complex& w , const Complex& z ){
    return w.compare(z) < 0 ;
}
bool operator==( const Complex& w , const Complex& z ){
    return w.compare(z) == 0 ;
}
bool operator!=( const Complex& w , const Complex& z ){
    return w.compare(z) != 0 ;
}
bool operator<=( const Complex& w , const Complex& z ){
    return w.compare(z) >= 0 ;
}
bool operator>=( const Complex& w , const Complex& z ){
    return w.compare(z) <= 0 ;
}


/** 
     Notes: You should attempt to implement the missing
            functions. E.g.
            * 'operator-' [binary], 'operator*', etc.
            * 'operator-=', 'operator*=', etc.
            * prefix and postfix decrement 'operator--'

            Also, you should try to justify all the 
            choices of member vs non-member and/or
            const vs non-const.
  */
