# Classes in C++ (revisited)

In this repository I try to "walk you" through some of the
topics we encounter while working with classes. Some of these
concepts might be familiar to you, while some of them might
be quite new. Regardless, you are encouraged to study all of 
the examples presented here. While it is true that there are 
lots and lots of lines of code here, most of them are do not
change from one _commit_ to the next one. Take advantage of
this fact as well as the tools available within this repository
to focus on specific concepts.

If what is presented here is still not quite clear, do not
hesitate to contact me or your TA, either during Office Hours,
or after lecture. Keep in mind that I am particularly slow
to respond to emails, so your best bet might be to talk to me
in person.

One more thing, please keep in mind that this is **a work in
progress*. If you find typos and or logic errors please let 
me know so that I update the files in this repository.
