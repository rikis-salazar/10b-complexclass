// Alternative file layout. 'complex.h' contains both,
// the definition, as well as the implementation of the
// Complex class. We'll see later that this setup is needed
// when working with "TEMPLATES".

// To test this setup, replace the 'complex.h' file from
// the directory 'ThreeFileLayout' with a copy of this file.

// In this particular example, small functions will be 
// "inlined". That is, the definition will be placed
// inside the class interface. One advantage of this
// approach is the fact that scrolling is not needed
// to fully access the code of a function. On the other
// hand, clarity might suffer as code is now found inside
// the interface.


// Include safeguards
#ifndef COMPLEX_10C_W17
#define COMPLEX_10C_W17


#include <iostream>  // std::cout in 'print()', 'to_console()'
#include <ostream>   // std::ostream in 'operator>>'


// Forward declaration
class Complex;



// Non-member function declarations
void print( const Complex& ); 
std::ostream& operator<<( std::ostream& , const Complex& );
Complex operator+( const Complex& , const Complex& );

// Logic operators
bool operator<( const Complex& , const Complex& );
bool operator>( const Complex& , const Complex& );
bool operator==( const Complex& , const Complex& );
bool operator!=( const Complex& , const Complex& );
bool operator<=( const Complex& , const Complex& );
bool operator>=( const Complex& , const Complex& );



// Class interface
class Complex{
  private: 
    double real, imaginary;

  public:
    // One constructor to rule them all.
    Complex( double x = 0., double y = 0.) 
        : real(x), imaginary(y) { }


    // Miscellaneous functions
    void to_console() const {
        std::cout << "Real: " << real 
                  << ", Imaginary: " << imaginary << ".\n";
        return;
    }
    void to_console2() const {
        std::cout << "Real: " << real_part() 
                  << ", Imaginary: " << imaginary_part() << ".\n";
        return;
    }


    // getters and setters
    double get_real() const {
        return real;
    }
    double get_imaginary() const {
        return imaginary;
    }
    void set_real( double x ) {
        real = x;
        return;
    }
    void set_imaginary( double y ) {
        imaginary = y;
        return;
    }


    // Combined functionality of getters and setters.
    double real_part() const {
        return real;
    }
    double& real_part() {
        return real;
    }
    double imaginary_part() const {
        return imaginary;
    }
    double& imaginary_part() {
        return imaginary;
    }


    // Binary Math operators (MEMBERS)
    Complex& operator+=( const Complex& z ) { 
        real += z.real;
        imaginary += z.imaginary;

        return *this; 
    }


    // Unary Math operators (MEMBERS)
    Complex operator-() const {
        return Complex( -real, -imaginary );
    }
    Complex& operator++() {
        return *this += 1;
    }
    Complex operator++( int unused ) {
        Complex copy_of_this = *this ;
        *this += 1;

        return copy_of_this;
    }


    // MEMBER helper functions.
    int compare( const Complex& ) const ;

    // NON-MEMBERS (friends)
    friend std::ostream& operator<<( std::ostream& , const Complex& );
};


// MEMBER helper functions
int Complex::compare( const Complex& w ) const {
    double A = real, B = imaginary;
    double C = w.real, D = w.imaginary;

    if ( A < C ) return 1;
    if ( (A == C) && (B < D) ) return 1;
    if ( (A == C) && (B == D) ) return 0;
    return -1;
}



// Extra NON-MEMBER function(s)
void print( const Complex& z ){ 
    z.to_console();
    return;
}



// Output operator
std::ostream& operator<<( std::ostream& out , const Complex& z) {
    out << z.real ;

    // If 'z.imaginary' is negative, use '-' instead of 
    // '+' and output the absolute value of 'z.imaginary'.
    if ( z.imaginary < 0 )
        out << " - " << -z.imaginary ;
    else
        out << " + " << z.imaginary ; 
            
    out << "i";

    return out;
}



// Non-member math operators
Complex operator+( const Complex& w, const Complex& z ){
    Complex copy_of_w( w );     
    return copy_of_w += z;
}



// Logic operators NON-MEMBER NON-FRIEND
bool operator<( const Complex& w , const Complex& z ){
    return w.compare(z) > 0 ;
}

bool operator>( const Complex& w , const Complex& z ){
    return w.compare(z) < 0 ;
}

bool operator==( const Complex& w , const Complex& z ){
    return w.compare(z) == 0 ;
}

bool operator!=( const Complex& w , const Complex& z ){
    return w.compare(z) != 0 ;
}

bool operator<=( const Complex& w , const Complex& z ){
    return w.compare(z) >= 0 ;
}

bool operator>=( const Complex& w , const Complex& z ){
    return w.compare(z) <= 0 ;
}

#endif
